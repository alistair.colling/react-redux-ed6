# React, Redux es6 :eyeglasses:


## Setup

`npm install`

`npm start` 
* Launches dev build
* Runs tests 
* Linting
* Starts server
* Opens browser with :fire: :gun:



## Uni-directional Data Flow

| **Data Flow** |
|---------------|
| React         |
| Action        |
| Reducer       |
| Store         |
| react-redux   |
| React         |
