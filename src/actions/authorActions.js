import AuthorApi from '../api/mockAuthor.api';
import * as types from './actionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';

export function loadAuthorsSuccess(authors) {
  return { type: types.LOAD_AUTHORS_SUCCESS, authors};
}

export function setSelectedAuthor(author) {
  return {type: types.SET_SELECTED_AUTHOR, author};
}

export function updateAuthorSuccess(authors) {
  return {type: types.UPDATE_AUTHOR_SUCCESS, authors};
}

export function createAuthorSuccess(authors) {
  return {type: types.CREATE_AUTHOR_SUCCESS, authors};
}

export function loadAuthors(){
  return dispatch => {
      // set the loading dots going...
      dispatch(beginAjaxCall());
    return AuthorApi.getAllAuthors().then(authors => {
      dispatch(loadAuthorsSuccess(authors));
    }).catch(error => {
      throw(error);
    });
  };
}

export function deleteAuthor(author) {
  return (dispatch, getState) =>{
    dispatch(beginAjaxCall());
    return AuthorApi.deleteAuthor(author.id).then(
      authors => {
        dispatch(updateAuthorSuccess(authors));
      }
    ).catch( error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}

export function saveAuthor(author) {
  return function  (dispatch, getState){
    dispatch(beginAjaxCall());

    return AuthorApi.saveAuthor(author).then(
      // TODO this condition below wil never trigger false as
      // `saveAuthor()` in mockAuthor.api.js generates a new id
      // for any author without an id
      savedAuthor => {
        author.id ? dispatch(updateAuthorSuccess(savedAuthor)) :
        dispatch(createAuthorSuccess(savedAuthor));
      }
    );
  };
}
