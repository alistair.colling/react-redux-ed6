import * as types from './actionTypes';
import courseApi from '../api/mockCourse.api';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';

export function loadCoursesSuccess(courses) {
  return { type: types.LOAD_COURSES_SUCCESS, courses};
}

export function createCourseSuccess(courses) {
  return { type: types.CREATE_COURSE_SUCCESS, courses};
}

export function updateCourseSuccess(courses) {
  return { type: types.UPDATE_COURSE_SUCCESS, courses};
}

export function loadCourses(){
  return function(dispatch){
    // set the loading dots going...
    dispatch(beginAjaxCall());
    return courseApi.getAllCourses().then(courses => {
      dispatch(loadCoursesSuccess(courses));
    }).catch(error => {
      throw(error);
    });
  };
}

export function deleteCourse(course) {
  return function (dispatch, state) {
    dispatch(beginAjaxCall());
    return courseApi.deleteCourse(course.id).then(
      courses => {
        dispatch(updateCourseSuccess(courses));
      }
    ).catch( error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}

export function saveCourse(course){
  return function(dispatch, getState){
    // set the loading dots going...
    dispatch(beginAjaxCall());

    return courseApi.saveCourse(course).then(
      courses => {
        course.id ? dispatch(updateCourseSuccess(courses)) :
        dispatch(createCourseSuccess(courses));
      }
    ).catch(error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}
