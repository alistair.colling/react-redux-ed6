import delay from './delay';

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.
let authors = [
  {
    id: 'cory-house',
    firstName: 'Cory',
    lastName: 'House'
  },
  {
    id: 'scott-allen',
    firstName: 'Scott',
    lastName: 'Allen'
  },
  {
    id: 'dan-wahlin',
    firstName: 'Dan',
    lastName: 'Wahlin'
  }
];

//This would be performed on the server in a real app. Just stubbing in.
const generateId = (author) => {
  return author.firstName.toLowerCase() + '-' + author.lastName.toLowerCase();
};

class AuthorApi {
  static getAllAuthors() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], authors));
      }, delay);
    });
  }

  static saveAuthor(author) {
    author = Object.assign({}, author); // to avoid manipulating object passed in.
    let newAuthors = [];

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // Simulate server-side validation
        const minAuthorNameLength = 3;
        if (author.firstName.length < minAuthorNameLength) {
          reject(`First Name must be at least ${minAuthorNameLength} characters.`);
        }

        if (author.lastName.length < minAuthorNameLength) {
          reject(`Last Name must be at least ${minAuthorNameLength} characters.`);
        }
        if (author.id) {
          const existingAuthorIndex = authors.findIndex(a => a.id == author.id);

          newAuthors = [
            ...authors.slice(0, existingAuthorIndex), author, ...authors.slice(existingAuthorIndex + 1)
          ];
        } else {
          //Just simulating creation here.
          //The server would generate ids for new authors in a real app.
          //Cloning so copy returned is passed by value rather than by reference.
          author.id = generateId(author);
          newAuthors = [
            ...authors,
            author
          ];
        }
        authors = newAuthors;
        resolve(authors);
      }, delay);
    });
  }

  static deleteAuthor(authorId) {
    let newAuthors = [];
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const indexOfAuthorToDelete = authors.findIndex(author => {
          return author.id == authorId;
        });

        /* authors.splice(indexOfAuthorToDelete, 1);*/
        newAuthors = authors.slice(0,indexOfAuthorToDelete).concat(authors.slice(indexOfAuthorToDelete+1));
        authors = newAuthors;
        resolve(authors);
      }, delay);
    });
  }
}

export default AuthorApi;
