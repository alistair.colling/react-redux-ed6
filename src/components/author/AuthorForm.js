import React from 'react';
import TextInput from '../../common/TextInput';
import SelectInput from '../../common/SelectInput';

const AuthorForm = ({ author, allAuthors, onSave, onChange, onDelete, onChangeDetails, saving, errors }) => {
  return(
    <form>
      <h1>Manage Author</h1>
      <SelectInput
        name="authorId"
        label="Author"
        value={author.id}
        options={allAuthors}
        defaultOption ={'Add new...'}
        onChange={onChange}
        error={errors.authorId} />
      <TextInput
        name="firstName"
        label="First Name"
        value={author.firstName}
        onChange={onChangeDetails}
        error={errors.title} />
      <TextInput
        name="lastName"
        label="Last Name"
        onChange={onChangeDetails}
        value={author.lastName}
        error={errors.title} />
      <div> 
    <button type="button"
      value={'Delete'}
      onClick={onDelete}
      className="btn btn-danger">Delete</button>
    <input
      type="submit"
      disabled={saving}
      value={saving ? 'Saving...' : 'Save'}
      className="btn btn-primary"
      onClick={onSave} />
      </div>
    </form>

  );
};

AuthorForm.propTypes = {
  author: React.PropTypes.object.isRequired,
  allAuthors: React.PropTypes.array.isRequired,
  onSave: React.PropTypes.func.isRequired,
  onDelete: React.PropTypes.func.isRequired,
  onChangeDetails: React.PropTypes.func.isRequired,
  onChange: React.PropTypes.func.isRequired,
  saving: React.PropTypes.bool,
  errors: React.PropTypes.object
};

export default AuthorForm;
