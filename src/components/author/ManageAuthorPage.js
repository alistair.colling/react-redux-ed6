import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as courseActions from '../../actions/courseActions';
import * as authorActions from '../../actions/authorActions';
import AuthorForm from './AuthorForm';
import toastr from 'toastr';
import {authorsFormattedForDropdown} from '../../selectors/selectors';
import { browserHistory } from 'react-router';

export class ManageAuthorPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      /* course: Object.assign({}, props.course),*/
      // TODO why is this not working below??
      /* authors: Object.assign({}, props.authors),*/
      author: Object.assign({}, props.author),
      errors :{},
      saving: false
    };

    this.changeSelectedAuthor = this.changeSelectedAuthor.bind(this);
    this.changeAuthorDetails = this.changeAuthorDetails.bind(this);
    this.saveAuthor = this.saveAuthor.bind(this);
    this.deleteAuthor = this.deleteAuthor.bind(this);
    this.redirect = this.redirect.bind(this);
  }

  // This lifecycle function is called any time that props have changed
  // as well as sany time that react *thinks* that props might have
  // changed

  componentWillReceiveProps(nextProps){

    // Only run this code is there has been a change i.e. once the data loads
    /* if (this.props.course.id != nextProps.course.id){
     *   this.setState({course: Object.assign({}, nextProps.course)});
     * }*/
    /* debugger;*/
    /* debugger;
     * if (this.props.author.id != nextProps.author.id){
     *   this.setState({author: Object.assign({}, nextProps.author)});
     * }*/
  }

  getBlankAuthor() {
    return { value:"add-new", id: "", firstName: "", lastName:""};
  }

  changeSelectedAuthor(event) {
    const selectedIndex = event.target.selectedIndex;
    // If the selected index is 0 then we are adding a new author
    let selectedAuthor = this.getBlankAuthor();
    if (selectedIndex > 0){
      selectedAuthor = this.props.authors[event.target.selectedIndex -1 ];
    }
    this.setState({author: selectedAuthor});
    browserHistory.push('/author/'+ selectedAuthor.value);
  }

  changeAuthorDetails(event) {
    const field = event.target.name;
    let author = this.state.author;
    author[field] = event.target.value;
    return this.setState({author: author});
  }

  authorFormIsValid() {
    let formIsValid = true;
    let errors = {};
    /* if (this.state.course.title.length < 5) {
       *   errors.title = 'Title must be at least 5 characters.';
       *   formIsValid = false;
       * }*/
    this.setState({ errors: errors });
    return formIsValid;
  }

  saveAuthor(event){
    event.preventDefault();

    /* if (!this.authorFormIsValid()) {
       *   return;
       * }*/
    this.setState({saving: true});
    this.props.actions.saveAuthor(this.state.author)
        .then(() => this.redirect())
        .catch(error => {
          toastr.error(error);
          this.setState({saving: false});
        });
  }

  authorHasCourses(author) {
    return this.props.courses.find( course => course.authorId === author.id);
  }

  deleteAuthor(event){
    event.preventDefault();

    /* if (!this.authorFormIsValid()) {
       *   return;
       * }*/
    if (this.authorHasCourses(this.state.author)) {
      toastr.error('This author has courses assigned');
      return;
    }
    this.setState({saving: true});
    this.props.actions.deleteAuthor(this.state.author)
        .then(() => this.redirect())
        .catch(error => {
          toastr.error(error);
          this.setState({saving: false});
        });
  }

  redirect(){
    this.setState({saving: false, author: this.getBlankAuthor()});
    toastr.success('Author saved / deleted');
    /* this.context.router.push('/author');*/
  }

  render() {
    return (
      <AuthorForm
        allAuthors={this.props.authors}
        author={this.state.author}
        onChange={this.changeSelectedAuthor}
        onChangeDetails={this.changeAuthorDetails}
        onSave ={this.saveAuthor}
        onDelete ={this.deleteAuthor}
        errors={this.state.errors}
        saving={this.state.saving}
      />
    );
  }

}

ManageAuthorPage.propTypes = {
  /* myProp: React.PropTypes.string.isRequired,*/
  /* course: React.PropTypes.object.isRequired,*/
  //TODO this could be made to check there is an array of objects
  author: React.PropTypes.any.isRequired,
  courses: React.PropTypes.object.isRequired,
  authors: React.PropTypes.array.isRequired,
  actions: React.PropTypes.object.isRequired
};

//Pull in the React Router so it is available on this.context.router

ManageAuthorPage.contextTypes = {
  router: React.PropTypes.object.isRequired
};

// This is the place where data from the API would be reformatted to suit
// the component

function getCourseById(courses, id){
  const course = courses.filter( course => course.id == id);
  if (course.length) return course[0];// we only want to return one course
  return null;
}

function getAuthorById(authors, id){
  const author = authors.filter( course => course.id == id);
  if (authors.length) return author[0];// we only want to return one course
  return null;
}

const mapStateToProps = (state, ownProps) => {
  //the course id is read from the URL here
  // This comes from the URL
  const authorId = ownProps.params.id;
  let author = {id:'', firstName: '', lastName: '' };

  // Lets get the course to populate the page
  // gets the author using the id from the url
  if (authorId && state.authors.length > 0) {
    author = getAuthorById(state.authors, authorId);
  }
  return {
    author: author,
    // This is a selector - it selects something from the current state
    // so it can be used by a component.
    // Dan Abramov recommends it can be stored in it's corresponding
    // reducer file though here we have 1 file for all selectors.
    // Both work though probs makes more sense for it to go
    // in the reducer file
    authors: authorsFormattedForDropdown(state.authors),
    courses: state.courses
  };
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authorActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps) ( ManageAuthorPage );
