import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as courseActions from '../../actions/courseActions';

// This is a container component i.e. connects to redux
import CourseList from './CourseList';
import {browserHistory} from 'react-router';

class CoursesPage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.redirectToAddCoursePage = this.redirectToAddCoursePage.bind(this);
  }

  // Every time there is a change in the input field the state of the component
  // is set to the value in the field. Nothing is disptached until onClickSave
  // is called

  redirectToAddCoursePage() {
    browserHistory.push('/course/addNew');
  }

  render() {
    const {courses} = this.props;

    return (
      <div>
        <h1>Courses</h1>
        <input type="submit"
          value="Add Course"
          className="btn btn-primary"
          onClick={this.redirectToAddCoursePage}/>
        <CourseList courses={courses}/>
      </div>
    );
  }

}

CoursesPage.propTypes = {
  courses: React.PropTypes.array.isRequired,
  actions: React.PropTypes.object.isRequired
};


// ---------------------- REDUX ---------------------- //

// ----------- mapStateToProps ----------- //
//
// *** MAPS THE STATE TO THE PROPS ON THE COMPONENT! *** //
//
// 1 This function maps the part of the redux store/state I want to
// expose on this component
// 2 This function subscribes this component to redux store updates
// for the 'courses' property
// 3 Each time the store updates mapStateToProps is called
// 4 mapStateToProps returns an object, each property on the object
// that is defined will become a property on the container component
// e.g. `state.courses` is mapped to `this.props.courses`
// 5 mapStateToProps defines what state is available on this component
// via props
// `state` that is passed to this function represents the state that
// is within the redux *store*

function mapStateToProps(state, ownProps){
  return {
    courses: state.courses
  };
}


// ----------- mapDispatchToProps ----------- //
//
// This function exposes all of the actions to the component
// by adding them to props, rather than targetting them directly like this:
/* this.props.dispatch(courseActions.createCourse(this.state.course));*/

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(courseActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps) ( CoursesPage );
