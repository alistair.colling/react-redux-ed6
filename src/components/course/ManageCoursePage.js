import {browserHistory} from 'react-router';
import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as courseActions from '../../actions/courseActions';
import * as authorActions from '../../actions/authorActions';
import CourseForm from './CourseForm';
import toastr from 'toastr';
import {authorsFormattedForDropdown} from '../../selectors/selectors';

export class ManageCoursePage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      // course is stored on the local state of the component
      // passed down from the props
      // when course is updated action is fired
      course: Object.assign({}, props.course),
      // TODO why is this not working below??
      /* authors: Object.assign({}, props.authors),*/
      errors:{},
      saving: false
    };

    this.updateCourseState = this.updateCourseState.bind(this);
    this.saveCourse = this.saveCourse.bind(this);
    this.deleteCourse = this.deleteCourse.bind(this);
    this.redirect = this.redirect.bind(this);
  }

  // This lifecycle function is called any time that props have changed
  // as well as sany time that react *thinks* that props might have
  // changed

  componentWillReceiveProps(nextProps){
    // Only run this code is there has been a change i.e. once the data loads
    if (this.props.course.id != nextProps.course.id){
      this.setState({course: Object.assign({}, nextProps.course)});
    }
  }

  updateCourseState(event) {
    const field = event.target.name;
    let course = this.state.course;
    course[field] = event.target.value;
    return this.setState({course: course});
  }


  courseFormIsValid() {
    let formIsValid = true;
    let errors = {};
    if (this.state.course.title.length < 5) {
      errors.title = 'Title must be at least 5 characters.';
      formIsValid = false;
    }
    this.setState({ errors: errors });
    return formIsValid;
  }


  deleteCourse(event){
    event.preventDefault();
    //redirect to courses page whatever
    this.context.router.push('/courses');

    this.setState({saving:true});
    this.props.actions.deleteCourse(this.state.course)
        .then(()=> this.redirect())
        .catch(error =>{
          toastr.error(error);
          this.setState({saving:false});
        });
  }

  saveCourse(event){
    event.preventDefault();

    if (!this.courseFormIsValid()) {
      return;
    }
    this.setState({saving: true});
    this.props.actions.saveCourse(this.state.course)
        .then(() => this.redirect())
        .catch(error => {
          toastr.error(error);
          this.setState({saving: false});
        });
  }

  redirect(){
    this.context.router.push('/courses');
    this.setState({saving: false});
    toastr.success('Course saved / deleted');
  }

  render() {
    return (
      <CourseForm
        allAuthors={this.props.authors}
        course={this.state.course}
        onChange={this.updateCourseState}
        onDelete={this.deleteCourse}
        onSave={this.saveCourse}
        errors={this.state.errors}
        saving={this.state.saving}
      />
    );
  }

}

ManageCoursePage.propTypes = {
  /* myProp: React.PropTypes.string.isRequired,*/
  course: React.PropTypes.object.isRequired,
  //TODO this could be made to check there is an array of objects
  authors: React.PropTypes.array.isRequired,
  actions: React.PropTypes.object.isRequired
};

//Pull in the React Router so it is available on this.context.router

ManageCoursePage.contextTypes = {
  router: React.PropTypes.object
};

// This is the place where data from the API would be reformatted to suit
// the component

function getCourseById(courses, id){
  const course = courses.filter( course => course.id == id);
  if (course.length) return course[0];// we only want to return one course
  return null;
}

const mapStateToProps = (state, ownProps) => {
  let courseId = ownProps.params.id;
  if (courseId === 'addNew') courseId = null;
  let course = {id:'', watchHref: '', title: '', authorId:'',
                length:'', category:''};

  // Lets get the course to populate the page
  if (courseId && state.courses.length > 0) {
    //we aren't storing the selected course
    course = getCourseById(state.courses, courseId);
  }

  return {
    authors: authorsFormattedForDropdown(state.authors),
    course: course
  };
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(courseActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps) ( ManageCoursePage );
