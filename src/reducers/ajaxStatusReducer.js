import * as types from '../actions/actionTypes';
import initialState from './initialState';
// This action is called each time an ajax call is made
// or completes

function actionTypeEndsInSuccess(type) {
  return type.substring(type.length - 8 ) == '_SUCCESS';
}

export default function ajaxStatusReducer(state = initialState.ajaxCallsInProgress, action){
  if(action.type == types.BEGIN_AJAX_CALL){
    return state +1;
  } else if
  // When a call has ended in either an error or success
  // reduce the total by 1
  (action.type == types.AJAX_CALL_ERROR ||
             actionTypeEndsInSuccess(action.type)){
    return state -1;
  }
  return state;
}
