import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function authorReducer(state = initialState.author, action) {
  // state passed should be author
  switch(action.type){
    case types.SET_SELECTED_AUTHOR :
      return [
        ...state,
        Object.assign({}, action.author)
      ];

    default:
      return state;
  }
}
