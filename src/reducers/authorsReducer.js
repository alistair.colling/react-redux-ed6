import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function authorsReducer(state = initialState.authors, action) {
  switch(action.type){
    case types.LOAD_AUTHORS_SUCCESS :
      return  action.authors;

    case types.UPDATE_AUTHOR_SUCCESS :
      // Saved author is returned from API
      // Use filter to iterate over the authors array passsed in *as* state
      // then if the id matches, replace it
      /* return state.push(action.author);
       * This ^ would be mutating state and so throws an error due to
       * redux-freeze-state package*/
      return action.authors;

    case types.CREATE_AUTHOR_SUCCESS :
      return action.authors;
    default:
      return state;
  }
}
