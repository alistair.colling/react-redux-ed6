import * as types from '../actions/actionTypes';
import initialState from './initialState.js';

export default function courseReducer(state = initialState.courses, action) {
  debugger;
  switch(action.type){
    case types.LOAD_COURSES_SUCCESS :
      return action.courses;

    case types.CREATE_COURSE_SUCCESS :
      return action.courses;
    case types.UPDATE_COURSE_SUCCESS :
      /**
       * This case first creates a copy of the course array (state only contains courses
         array in this reducer). The exploded ... array is then filtered to return all
         items **except** the course we are upadating (i.e. the course id does not match).
         We then create a new object and assign the updated course to it so that a
         clone is made (i.e. we're not directly accessing the updated course)
       */
      // creates a new array with 
      // Needs to return an array
      return action.courses;

    default:
      return state;
  }
}
