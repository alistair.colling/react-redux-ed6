import {combineReducers} from 'redux';
import courses from './courseReducer';
import authors from './authorsReducer';
import author from './authorReducer';
import ajaxCallsInProgress from './ajaxStatusReducer';

// This is the place where all of the reducers are registered
// The rootReducer is passed when configureStore() is called index
// index.js
// The courses reducer in turn creates a courses property on the
// store / state

const rootReducer = combineReducers({
  courses,
  authors,
  author,
  ajaxCallsInProgress
});

export default rootReducer;
