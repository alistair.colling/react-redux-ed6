export function authorsFormattedForDropdown( authors ){
  return authors.map( author => {
    return {
      id: author.id,
      value: author.id,
      firstName: author.firstName,
      lastName: author.lastName,
      text: author.firstName + ' ' + author.lastName
    };
  });
}
