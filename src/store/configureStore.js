import {createStore, applyMiddleware} from 'redux';
import rootReducer from '../reducers/';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';

// * This is called when the application is launched (from index.js)
// initialState is not passed although it could be e.g. from an API
// * The store is being told: Here is the root reducer that contains all
// of the other reducers.
// * These reducers kind of create properties on the store / state
// e.g. courses - courses in reducers/index.js.
// * The components subscribe to changes in each of these properties.
// * Subscriptions are defined in mapStateToProps in each component
import thunk from 'redux-thunk';
import freezeState from 'redux-freeze-state';

export default function configureStore( initialState ){
  return createStore(
    freezeState(rootReducer),
    initialState,
    //TODO I'm not quite sure what this does!
    applyMiddleware(thunk, reduxImmutableStateInvariant())
  );
}
